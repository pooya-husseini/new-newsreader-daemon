abnanews {
    name = "خبرگزاری اهل بیت"
    urls = ["http://fa.abna24.com/rss"]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = ["div[itemprop=articleBody]>p", "blockquote"]
}
//abrarnews {
//    name = "روزنامه ابرار"
//    urls = ["http://home.abrarnews.com/rss.xml"]
//    dateFormat = "E, dd MMM yyyy HH:mm:ss"
//    selects = ["div.boxcontent>ul"]
//}
aftabnews {
    name = "پایگاه خبری آفتاب"
    urls = ["http://aftabnews.ir/fa/rss/allnews"]
    dateFormat = "dd MMM yyyy HH:mm:ss"
    selects = [
            "div[style=text-align: justify;]",
            "div[style^=text-align: justify]",
            "div[align=justify]",
            "div.body>div",
            "div.body",
            "div.subtitle"
    ]
}
aftabyazd {
    name = "روزنامه آفتاب یزد"
    urls = ["http://aftabeyazd.ir/rss.xml"]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = ["div.fullstory-text>bfont"]
}
//alvefaqnews {
//    name = "الوفاق"
//    urls = ["http://www.al-vefagh.com/news/feed"]
//    dateFormat = "E, dd MMM yyyy HH:mm:ss"
//    selects = ["div.itemcontent>p"]
//}
//armannews { not readable
//    name = "روزنامه آرمان"
//    urls = ["http://www.armandaily.ir/Modules/RSS/Default.aspx"]
//    dateFormat = "E, dd MMM yyyy HH:mm:dd"
//    selects = ["span.lblBody"]
//}
asreiran {
    name = "خبرگزاری عصر ایران"
    urls = ["http://www.asriran.com/fa/rss/allnews"]
    dateFormat = "dd MMM yyyy HH:mm:ss"
    selects = [
            "div.body",
            "div.body>p",
            "div.body>div",
            "span[lang=FA]",
            "div[align=justify]",
            "div.body>span>span>p",
            "div[dir=RTL]",
            "p[dir=RTL]",
            "div[dir=RTL]>p",
            "div.body>p>span",
            "div.body>div.itemFullText>p>span"
    ]
}
//banifilm {
//    name = "بانی فیلم"
//    urls = ["http://www.banifilm.ir/Modules/RSS/Default.aspx"]
//    dateFormat = "MM/dd/yyyy hh:mm:ss a"
//    selects = [
//            "div.nduc-body>span>p"
//    ]
//}

//barackobama {
//    name = "خبرگزاری باراک اوباما"
//    urls = ["http://www.barackobama.ir/en/rss/2"]
//    dateFormat = "dd MMM yyyy HH:mm:ss"
//    selects = ["div.body"]
//}
borna {
    name = "خبرگزاری برنا"
    urls = ["http://www.bornanews.ir/feeds/"]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    followRedirects = true
    selects = [
            "p.news-lead",
            "div.article-body>p",
            "div.article-body>div",
            "div.article-body>p>span",
            "div.article-body>div>div>p",
            "div.article-body>div>div>p>span>span"
    ]
}
boursenews {
    name = "بورس نیوز"
    urls = ["http://www.boursenews.ir/fa/rss/allnews"]
    dateFormat = "dd MMM yyyy HH:mm:ss"
    selects = [
            "div.subtitle",
            "div[style^=text-align: justify]",
            "p[style^=text-align: justify]",
            "div[style^=text-align: justify]>span",
            "p[style^=text-align: justify]>span",
            "div[align=justify]",
            "p[align=justify]",
            "div[align=justify]>span",
            "p[align=justify]>span",
            "p[align=justify]",
            "span[lang=FA]",
            "div.body>p>span",
            "div.body>div>span>p",
            "div.body>div",
            "div.body>div>div",
            "div.body>div>div>p",
            "div.body>div>div>p>span",
            "div.body>ul>span",
            "div.body>div>p",
            "div.body>p",
            "div.x_fontlbltxt>div>span"
    ]
}
donyaye_eghtesad {
    name = "دنیای اقتصاد"
    urls = ["http://www.donya-e-eqtesad.com/general-news/rss/"]
    dateFormat = "MM/dd/yyyy hh:mm:ss a"
    selects = [
            "div.textform>div>p",
            "div.NewsText>p",
            "div.NewsText>p>strong"
    ]
}

//econews {
//    name = "خبرگزاری اقتصادی ایران"
//    urls = ["http://www.econews.ir/fa/rss.aspx?kind=-1"]
//    dateFormat = "E, dd MMM yyyy HH:mm:ss"
//    selects = [
//            "div.BodyText>p",
//            "div.item-body",
//            "div[style^=text-align: justify]"
//    ]
//}
eghtesad_online {
    name = "اقتصاد آنلاین"
    urls = ["http://www.eghtesadonline.com/fa/rss"]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = [
            "div.df-lead>p",
            "div.df-text>p",
            "div.body>p",
            "div.body>span",
            "div.df-lead>p",
            "p.lead",
            "div.body>div"
    ]
}
entekhab {
    name = "پایگاه خبری تحلیلی انتخاب"
    urls = [
            "http://www.entekhab.ir/fa/rss/allnews",
            "http://www.entekhab.ir/fa/rss/5"
    ]
    dateFormat = "dd MMM yyyy HH:mm:ss"
    selects = [
            "div[align=justify]",
            "div[align=justify]>span",
            "p[align=justify]",
            "div.body>p",
            "div.body>span",
            "div.body",
            "div.body>div",
            "div.body>div>div",
            "span[lang=FA]",
            "p[dir=RTL]"
    ]
}

//etemad {
//    name = "روزنامه اعتماد"
//    urls = ["http://www.etemadnewspaper.ir/Modules/RSS/Default.aspx?type=Main"]
//    dateFormat = "E, dd MMM yyyy HH:mm:dd"
//    selects = [
//            "div.MiddleTop_Matn",
//            "div.NewsDetail-Body>p"
//    ]
//}

farda {
    name = "فردا"
    urls = ["http://www.fardanews.com/fa/rss/allnews"]
    dateFormat = "dd MMM yyyy HH:mm:ss"
    selects = [
            "div.excerpt>p",
            "div.body>div",
            "div.content",
            "div.body"]
}
fararu {
    name = "خبرگزاری فرارو"
    urls = ["http://fararu.com/fa/rss/allnews"]
    dateFormat = "dd MMM yyyy HH:mm:ss"
    selects = [
            "div.body",
            "div.body>div"
    ]
}
fars {
    name = "خبرگزاری فارس"
    urls = ["http://www.farsnews.com/rss.php"]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = [
            "p.rtejustify",
            "p.nwstxtBodyPane>p",
            "span#nwstxtBodyPane>p",
            "div.nwstxtinfotitle",
            "p.nwstxttext"
    ]
}

ghanun {
    name = "روزنامه قانون"
    urls = ["http://www.ghanoondaily.ir/fa/Feed/Service/0"]
    dateFormat = "MM/dd/yyyy hh:mm:ss a"
    selects = [
            "span.NewsDetail-Body>p",
            "h2.newslid",
            "span.newsbody>p",
            "span.newsbody>div>div>p"
    ]
}
//hayat {
//    name = "حیات"
//    urls = ["http://hayat.ir/?a=content.q.rss&lang=fa"]
//    dateFormat = "E, dd MMM yyyy HH:mm:ss"
//    selects = [
//            "div#content-full>div>span",
//            "div#content-full>p>span",
//            "div#content-full>div"
//    ]
//}
hamshahri_online {
    name = "روزنامه همشهری"
    urls = [
            "http://www.hamshahrionline.ir/rss",
            "http://www.hamshahrionline.ir/rss/service/weather",
            "http://www.hamshahrionline.ir/rss/service/education",
            "http://www.hamshahrionline.ir/rss/service/society",
            "http://www.hamshahrionline.ir/rss/service/communication",
            "http://www.hamshahrionline.ir/rss/service/economy",
            "http://www.hamshahrionline.ir/rss/service/hamshahri",
            "http://www.hamshahrionline.ir/rss/service/thoughts"
    ]
    dateFormat = "dd MMM yyyy HH:mm:ss"
    selects = [
            "div.leadContainer",
            "div.body>p",
            "div.newsBodyCont>p"
    ]
}
iana {
    name = "خبرگزاری کشاورزی ایران"
    urls = ["http://www.iana.ir/archive.feed"]
    dateFormat = "dd MMM yyyy HH:mm:ss"
    selects = [
            "div.itemBody>div>p",
            "div.body>p>span",
            "div.body>span>p",
            "div.body>p",
            "div.body>div"
    ]
}
// ibna = invalid rss
// icana =invalid rss

//ikna {
//    name = "خبرگزاری بین المللی قرآن"
//    urls = ["http://www.iqna.ir/fa/rss/0"]
//    dateFormat = ""
//    selects = []
//}

ilna {
    name = "ایلنا : خبرگزاری کار ایران"
    urls = ["http://www.ilna.ir/feeds/"]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = [
            "div[align=justify]>p",
            "p[dir=RTL]",
            "section[itemprop=articlebody]>p",
            "section.articlebody>p",
            "section.article_body>div>p",
            "section.article_body>p",
            "section.article_body>div>p>span>span",
            "p[style^=text-align:justify]"
    ]
}
iran_daily {
    name = "Iran Daily"
    urls = ["http://www.iran-daily.com/News/Feed"]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = ["div.itemcontent>p"]
}
iran_economist {
    name = "ایران اکونومیست"
    urls = ["http://iraneconomist.com/fa/rss/allnews"]
    dateFormat = "dd MMM yyyy HH:mm:ss"
    selects = [
            "div.body",
            "div.body>div",
            "div.body>div>div",
            "div.body>p",
            "div[align=justify]",
            "div.body>span"
    ]
}
//sobhe_eghtesad {
//    name = "صبح اقتصاد"
//    urls = ["http://www.irannewsdaily.com/Site/Pages/CMSRSS.aspx?type=CMSNews"]
//    dateFormat = ""
//    selects = []
//}

//irib {
//    name = "واحد مرکزی خبر"
//    urls = ["http://iribnews.ir/RSS/RSS_AllNews.aspx"]
//    dateFormat = "E, dd MMM yyyy HH:mm:ss"
//    selects = ["span[id=MainContent_GV_NewsText_Label2_0]>font", "td.body>span"]
//}
irna {
    name = "ایرنا : خبرگزاری جمهوری اسلامی"
    urls = ["http://irna.ir//fa/rss.aspx?kind=-1",
            "http://irna.ir//fa/rss.aspx?kind=20"]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = [
            "div.BodyText>p",
            "div.ContentStyle>h3",
            "div.NewsContent>div>h3"
    ]
}
isna {
    name = "خبرگزاری دانشجویان ایران - ایسنا"
    urls = ["http://isna.ir/fa/all/feed"]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = [
            "div.body>p",
            "p.summary",
            "div.content-full-news>div>p"
    ]
}
//jahaneghtesad {
//    name = "جهان اقتصاد"
//    urls = ["http://www.jahaneghtesad.com/%D8%AE%D8%A8%D8%B1%D9%87%D8%A7?format=feed"]
//    dateFormat = "E, dd MMM yyyy HH:mm:ss"
//    selects = ["div.itemFullText>p"]
//}
jamejam {
    name = "روزنامه جام جم"
    urls = ["http://jamejamonline.ir/RssFeed/01", "http://jamejamonline.ir/RssFeed/05"]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = [
            "div.lid",
            "div.newsBody>div",
            "div.NewsContent>p",
            "div.newsBody>p",
            "div.full-text>p",
            "div.newsBody>div>p",
            "div.body-Preview>p",
            "div.body-Preview>p",
            "div.bodyContainer>p",
            "div.previewBody>p",
            "div.body>p",
            "div.headerNewsContainer>div>p"
    ]
}
jomhuri_eslami {
    name = "روزنامه جمهوری اسلامی"
    urls = [
            "http://jomhourieslami.com/rss.xml"
    ]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = [
            "span.fullstory-text>bfont",
            "div.fullstory-text>bfont"
    ]
}
kayhan {
    name = "روزنامه کیهان"
    urls = ["http://kayhan.ir/fa/rss/allnews"]
    dateFormat = "dd MMM yyyy HH:mm:ss"
    selects = [
            "div.subtitle",
            "div.body"
    ]
}
khabar_online {
    name = "خبر آنلاین"
    urls = ["http://khabaronline.ir/RSS"]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = [
            "div.leadCont>p",
            "div.body>p>a",
            "div.body>p",
            "div.body>p>span",
            "div[align=justify]>p",
            "div.newsBodyCont>div",
            "div.newsBodyCont>p"
    ]
    // it's date is empty
}
mashregh {
    name = "خبرگزاری مشرق"
    urls = ["http://www.mashreghnews.ir/fa/rss/allnews"]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = [
            "div.summary",
            "div.item-body>div>p",
            "h1.title>a",
            "p.introtext",
            "div.body>div",
            "div.body",
            "div.subtitle"

    ]
}
mehr {
    name = "خبرگزاری مهر"
    urls = [
            "http://www.mehrnews.com/rss-homepage",
            "http://www.mehrnews.com/rss?tp=537",
            "http://www.mehrnews.com/rss?tp=44",
            "http://www.mehrnews.com/rss?tp=122",
            "http://www.mehrnews.com/rss?tp=46",
            "http://www.mehrnews.com/rss?tp=520",
            "http://www.mehrnews.com/rss?tp=48",
            "http://www.mehrnews.com/rss?tp=51",
            "http://www.mehrnews.com/rss?tp=57"
    ]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = [
            "p[dir=RTL]",
            "div.item-title>h2",
            "div.full-text>p",
            "div.full-text>p>span",
            "div.full-text>p>span>strong"
    ]
}
//miras_farhangi {
//    name = "خبرگزاری میراث فرهنگی"
//    urls = ["http://www.chtn.ir/DesktopModules/DnnForge%20-%20NewsArticles/Rss.aspx?TabID=84&ModuleID=409&MaxCount=25"]
//    dateFormat = "dd MMM yyyy HH:mm"
//    followRedirects=true
//    selects = ["div.NewsBody>div",
//               "div.NewsBody>p"]
//}

moj {
    name = "خبرگزاری موج"
    urls = ["http://www.mojnews.com/feeds/"]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = [
            "div.mjFullView_Body_ContentTextL3>span",
            "div.article-body>div>div>p",
            "div.article-body>div>p"
    ]
}
navad {
    name = "نود"
    urls = ["http://navad.net/rss/RSS.xml"]
    dateFormat = "MM/dd/yyyy hh:mm:ss"
    selects = [
            "div.bodynews>p",
            "div.leadnews",
            "div.bodynews>p>b>span",
            "div.bodynews>p>span"
    ]
}
//pana { // todo link is on tag attribute
//    name = "خبرگزاری پانا"
//    urls = ["http://pana.ir/rss/all"]
//    dateFormat = "yyyy-MM-ddTHH:mm:ss"
//    selects = ["div#newsbody>p", "div#newsbody"]
//
//}
resalat {
    name = "روزنامه رسالت"
    urls = ["http://www.resalat-news.com/Fa/Rss.aspx"]
    selects = [
            "span#View1_lblText>div",
            "span#View1_lblText>p"
    ]
    dateFormat = "E MMM dd yyyy HH:mm:ss"

}
sena {
    name = "پایگاه اطلاع رسانی بازار سرمایه ایران"
    urls = ["http://sena.ir/RSS.ashx"]
    dateFormat = "E MMM dd yyyy HH:mm:ss"
    selects = [
            "p[dir=RTL]>span",
            "span#ctl00_ContentPlaceHolder1_lblSumm",
            "span#ctl00_ContentPlaceHolder1_lblDescript>p>span",
            "p[dir=rtl]>em>span",
            "td[align=right]>span>em>span"
    ]
}
shana {
    name = "شبکه اطلاع رسانی نفت و انرژی"
    urls = ["http://www.shana.ir/Components/NewsAgency/View/Rss.aspx?imagesize=120&topcount=1000&lang=fa&id=0"]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = [
            "li.newsBody>span>p",
            "div.naNewsBody>span>div",
            "div.naNewsBody>div>span",
            "div.naNewsBody>",
            "div.naNewsBody>div",
            "div.naNewsLead>h4",
            "li.newsBody>span",
            "div.lead>span"
    ]
}
//sina {
//    name = "خبرگزاری علم وفرهنگ"
//    urls = ["http://sinapress.ir/rss/49"]
//    dateFormat = ""
//    selects = []
//}
taamol {
    name = "خبرگزاری تعامل"
    urls = ["http://www.taamolnews.ir/fa/rss/allnews"]
    dateFormat = "dd MMM yyyy HH:mm:ss"
    selects = [
            "div.body>div>p>span",
            "div.body>div",
            "div.body>p",
            "div.body",
            "div.body>p>span",
            "div.body>div>span",
            "div.body>span",
            "div.body>span>span",
            "div.NewsPartBody>p>span"
    ]
}
//tabnak {
//    name = "سایت خبری تحلیلی تابناک"
//    urls = ["http://www.tabnak.ir/fa/rss/allnews"]
//    dateFormat = "dd MMM yyyy HH:mm:ss"
//    selects = [
//            "div.body",
//            "div.body>p",
//            "div.body>div",
//            "div.full-text>p",
//            "div.itemFullText>p",
//            "span[lang=FA]",
//            "span[lang=AR-SA]",
//            "div.divItem>span>p",
//            "div.divItem>span>p>strong"
//    ]
//}

//tafahom {
//    name = "تفاهم"
//    urls = ["http://www.tafahomnews.com/Modules/RSS/Default.aspx?type=Main"]
//    dateFormat = "E, dd MMM yyyy HH:mm:ss"
//    selects = ["span.lblBody>div"]
//}
//tse { //todo append site to the link in rss
//    name = "بورس و اوراق بهادار تهران"
//    urls = ["http://new.tse.ir/news/rss.xml"]
//    dateFormat = "MM/dd/yyyy hh:mm:ss a"
//    selects = [
//            "div#newBody>p",
//            "div#newsQuote",
//            "div#newsBody>p"
//    ]
//}
vatane_emrooz {
    name = "وطن امروز"
    urls = ["http://www.vatanemrooz.ir/newspaper/feed"]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = ["div.body>p"]
}
yjc {
    name = "باشگاه خبرنگاران جوان"
    urls = ["http://www.yjc.ir/fa/rss/allnews"]
    dateFormat = "dd MMM yyyy HH:mm:ss"
    readHttpFullContent = true
    selects = [
            "div[align=justify]",
            "div.body",
            "div.body>div",
            "div.body>div>span",
            "div.body>font",
            "div.body>span",
            "div.body>div",
            "div.body>div>span",
            "div.body>div>div>span",
            "div.body>p",
            "span[lang=FA]",
            "div[style^=text-align: justify]",
            "div[style^=text-align: justify]>div>span",
            "div[style^=text-align: justify]>span",
            "span[style^=text-align: justify]",
            "span[style^=font-size]",
            "span[id=MainContent_GV_NewsText_Label2_0]",
            "div[style^=text-align: justify]>span",
            "span[lang=AR-SA]"
    ]
}
leader {
    name = "حضرت آيت‌الله‌ سيدعلی خامنه‌ای"
    urls = ["http://farsi.khamenei.ir/rss"]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = [
            "div.Content>p",
            "div.Content>div>span",
            "div.Content>div"
    ]
}
//shora_tehran {
//    name = "شورای شهر تهران"
//    urls = ["http://www.shoratehran.ir/rss/cats/1"]
//    dateFormat = "dd MMM, yyyy HH:mm:ss"
//    selects = ["div.contents>p"]
//}
vana {
    name = "آژانس خبری ورزش ایران"
    urls = ["http://vananews.ir/fa/rss/allnews"]
    dateFormat = "dd MMM yyyy HH:mm:ss"
    selects = [
            "div.body",
            "div.subtitle",
            "div.body>p",
            "div.body>div",
            "div.body>div>div>p>span",
            "div.body>div>div>p>span>strong",
            "div.body>span"
    ]
}
basij {
    name = "خبرگزاری بسیج"
    urls = ["http://basijnews.ir/fa/rss/allnews"]
    dateFormat = "dd MMM yyyy HH:mm:ss"
    selects = [
            "div.subtitle",
            "div.title>h1>a",
            "div.body>div>p>span",
            "div.body>div>div>span",
            "div.body>span",
            "div.body>p",
            "div.body>div",
            "div.body",
            "div.body>p>span"
    ]
}

//itna = invalid rss

//farazna {
//    name = "خبرگزاری فر ایران زمین"
//    urls = ["http://farazna.ir/News/single/39417/"]
//    dateFormat = "E, dd MMM yyyy HH:mm:ss"
//    selects = ["div.sng>div>p"]
//}

shahr {
    name = "شهر"
    urls = ["http://shahr.tehran.ir/DesktopModules/DNNArticle/DNNArticleRSS.aspx?portalid=0&moduleid=531&tabid=135&categoryid=-1&cp=False&uid=-1&Language=fa-IR"]
    dateFormat = "E, dd MMM yyyy HH:mm:ss"
    selects = [
            "div.body>div",
            "div.content",
            "div.summaryDiv2",
            "div.contentnews>div",
            "div.contentnews"
    ]
}


arya {
    name = "خبرگزاری آریا"
    urls = ["http://aryanews.com/Feed.aspx?l=fa-ir"]
    dateFormat = "yyyy/MM/dd HH:mm:ss"
    selects = [
            "tbody>tr>td>div>p",
            "tbody>tr>td>div>div>div",
            "tbody>tr>td>div>div>div>div",
            "tbody>tr>td>div>div",
            "tbody>tr>td>div",
            "tbody>tr>td>div>div>div>span",
            "div.main-news"
    ]
}
//yalasarat = invalid rss
javan {
    name = "خبرگزاری جوان"
    urls = ["http://javanonline.ir/fa/rss/allnews"]
    dateFormat = "dd MMM yyyy HH:mm:ss"
    selects = [
            "div.subtitle",
            "h1.title",
            "div.body>span>p",
            "div.body>div",
            "div.body"
    ]
}