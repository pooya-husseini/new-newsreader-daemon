package ir.phsys.sitereader.messages

import ir.phsys.sitereader.Content
import ir.phsys.sitereader.solr.ContentBean
import ir.phsys.sitereader.xml.NewsRSSTag

/**
 * @author Pooya husseini on 9/3/15.
 *         for more information about the author @see pooya-hfp.ir
 */
object Messages {

  case class CommitBeans(beans: List[ContentBean], remainAgencies: List[String], startedTime: Long)

  case class CommitmentFailed(t: Throwable, remainAgencies: List[String], startedTime: Long)

  case class OperationSucceed(remainAgencies: List[String], startedTime: Long)

  case class CommitmentSucceed(remainAgencies: List[String], startedTime: Long)

  case class FilterNotExistKeys(keys: Map[String, Any], remainAgencies: List[String], startedTime: Long)

  case class FilteredKey(keys: Map[String, Any], remainAgencies: List[String], startedTime: Long)

  trait Operation

  case class Index(agencyTag: String, remainAgencies: List[String], startedTime: Long)

  case object Optimize

  case object IndexAll

  case class TotalOperationSucceed(remainAgencies: List[String], startedTime: Long)

  case class TotalOperationFailed(t: Throwable, remainAgencies: List[String], startedTime: Long)

  case class GiveTagUrls(tag: String, remainAgencies: List[String], startedTime: Long)

  trait ContentMakingResult

  case class MakeContent(tags: List[(NewsRSSTag, String)], remainAgencies: List[String], startedTime: Long)

  case class ContentMakingFinished(contents: List[Content], remainAgencies: List[String], startedTime: Long) extends ContentMakingResult

  case class ContentMakingFailed(throwable: Throwable, remainAgencies: List[String], startedTime: Long) extends ContentMakingResult

  case class StartIndex(agencyTag: String, tags: List[(NewsRSSTag, String)], remainAgencies: List[String], startedTime: Long)

  case class AgencyUrls(tag: String, xs: List[String], remainAgencies: List[String], startedTime: Long)

}
