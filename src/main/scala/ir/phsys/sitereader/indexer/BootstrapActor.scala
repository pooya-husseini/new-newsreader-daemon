package ir.phsys.sitereader.indexer

import akka.actor.{Actor, ActorRef, Props}
import ir.phsys.sitereader._
import ir.phsys.sitereader.engine.solr.SolrServer
import ir.phsys.sitereader.exception.CannotIdentifyMessageException
import ir.phsys.sitereader.indexer.BootstrapActor._
import ir.phsys.sitereader.solr.ContentBean
import ir.phsys.sitereader.xml.NewsRSSTag
import org.apache.logging.log4j.LogManager

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}
import scala.xml.XML

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: ${Date}
 *         Time: ${time}
 */

object BootstrapActor {
  private lazy val LOGGER = LogManager.getLogger(this.getClass)

  lazy val props = Props(new BootstrapActor)
}


class BootstrapActor extends Actor {

  import messages.Messages._

  lazy val siteReaderActor: ActorRef = {
    context.actorOf(SiteReaderActor.props, "siteReaderActor")
  }
  lazy val solrServerActor: ActorRef = {
    context.actorOf(SolrServer.props, "SolrServerActor")
  }

  override def receive: Receive = {
    case Optimize=>
      solrServerActor!Optimize
    case AgencyUrls(tag, xs, remains, startedTime) =>
      Try {
        makeRssTags(xs, tag)
      } match {
        case Success(x) =>
          LOGGER.info(s"Rss of $tag already proccessed!")
          siteReaderActor ! StartIndex(tag, x, remains, startedTime)
        case Failure(t) =>
          context.parent ! TotalOperationFailed(t, remains, startedTime)
      }


    case Index(agency, remains, startedTime) =>
      siteReaderActor ! GiveTagUrls(agency, remains, startedTime)
    case ContentMakingFinished(xs, remains, startedTime) =>
      val beans = xs.collect {
        case SiteContent(title, id, content, publisher, link, url, pubdate) =>
          new ContentBean(id, title, content, content, link, publisher, url, pubdate, true, "success")
        case EmptySiteContent(title, id, publisher, link, url, pubdate) =>
          new ContentBean(id, title, "", "", link, publisher, url, pubdate, false, "empty")
        case FailedSiteContent(title, id, content, publisher, link, url, pubdate) =>
          new ContentBean(id, title, content, content, link, publisher, url, pubdate, false, "failed")
      }

      solrServerActor ! CommitBeans(beans, remains, startedTime)
    case ContentMakingFailed(t, remains,startedTime) =>
      context.parent ! TotalOperationFailed(t, remains,startedTime)
    case CommitmentSucceed(remains,startedTime) =>
      context.parent ! TotalOperationSucceed(remains,startedTime)
    case CommitmentFailed(t, remains,startedTime) =>
      context.parent ! TotalOperationFailed(t, remains,startedTime)
    case OperationSucceed(remains,startedTime) =>
      context.parent ! OperationSucceed(remains,startedTime)
    case x => throw CannotIdentifyMessageException(s"Can not identify $x from ${sender()}")
  }

  private def makeRssTags(urls: List[String], agencyTag: String): List[(NewsRSSTag, String)] = {
    val rss = urls.map(x => Future {
      (x, XML.load(x))
    })
    Try {
      Await.result(Future.sequence(rss), 1 minutes).flatMap(x => {
        val node = x._2 \\ "rss" \\ "channel" \\ "item"

        node.map(innerNode => (NewsRSSTag((innerNode \\ "link").map(_.text).mkString, {val pubDate=(innerNode \\ "pubDate").map(_.text).mkString; if(pubDate==null || pubDate.isEmpty) (innerNode \\ "updated").map(_.text).mkString else pubDate}, (innerNode \\ "title").map(_.text).mkString, agencyTag), x._1))
      })
    } match {
      case Success(x) =>
        x
      case Failure(t) =>
        LOGGER.error("Exception occurred on making rss of " + agencyTag, t)
        throw t
    }
  }
}