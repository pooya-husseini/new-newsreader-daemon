package ir.phsys.sitereader.indexer

import java.io.IOException

import akka.actor.{Actor, ActorInitializationException, ActorKilledException, ActorRef, ActorSystem, Cancellable, OneForOneStrategy, Props, SupervisorStrategy, Terminated}
import akka.actor.SupervisorStrategy.Decider
import akka.actor.SupervisorStrategy.Escalate
import akka.actor.SupervisorStrategy.Restart
import akka.actor.SupervisorStrategy.Stop
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import ir.phsys.sitereader.SiteReaderActor
import ir.phsys.sitereader.messages.Messages.Index
import ir.phsys.sitereader.messages.Messages.IndexAll
import ir.phsys.sitereader.messages.Messages.Optimize
import ir.phsys.sitereader.messages.Messages.TotalOperationFailed
import ir.phsys.sitereader.messages.Messages.TotalOperationSucceed
import org.apache.logging.log4j.LogManager

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Await
import scala.concurrent.Future
import scala.language.postfixOps
import scala.util.Random
import IndexRunner.{conf, logger}

import scala.concurrent.duration._

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 27.04.15
 *         Time: 08:45
 */

object IndexRunner {

  private val logger = LogManager.getLogger(this.getClass)
  val conf: Config = ConfigFactory.parseString {
    """
      |akka {
      |  loglevel = "WARNING"
      |  actor {
      |   debug{
      |     receive=on
      |   }
      |  }
      |}
    """.stripMargin
  }
}

class IndexRunner {

  private implicit val system = ActorSystem("CaspSpecActor", ConfigFactory.load(conf))

  final val defaultStrategy: SupervisorStrategy = {
    def defaultDecider: Decider = {
      case _: ActorInitializationException => Stop
      case _: ActorKilledException => Stop
      case _: Exception => Restart
    }
    OneForOneStrategy()(defaultDecider)
  }

  //  private implicit val timeout = Timeout(10 minute)

  def init(): Unit = {
    val tags = SiteReaderActor.loadReaders.map(_.tag)
    val actor = system.actorOf(Props(new Actor {
      override def supervisorStrategy: SupervisorStrategy = {
        OneForOneStrategy(3, 3 minutes, loggingEnabled = true) {
          case _: IOException => Restart
          case _: Exception => Escalate
        }
      }

      lazy val projectActor: ActorRef = {
        val props = BootstrapActor.props
        context.actorOf(props, "bootstrap")
      }

      def sendRemains(remains: List[String], startedTime: Long): Unit = remains match {
        case Nil =>
          logger.info(s"Indexing totally finished in ${System.currentTimeMillis() - startedTime} milliseconds")
          self ! IndexAll
        case x :: xs =>
          projectActor ! Index(x, xs, startedTime)
      }

      override def receive: Receive = {
        case IndexAll =>
          logger.info("Scheduler has been started!")
          System.gc()
          val shuffled = Random.shuffle(tags)
          projectActor ! Optimize
          projectActor ! Index(shuffled.head, shuffled.tail, System.currentTimeMillis())
        case TotalOperationFailed(t, remains, startedTime) =>
          logger.error("Error occurred in whole operation", t)
          sendRemains(remains, startedTime)
        case TotalOperationSucceed(remains, startedTime) =>
          sendRemains(remains, startedTime)
        case x:Any => logger.error(x + " received")
      }
    }
    ))

    val schedule: Cancellable = system.scheduler.scheduleOnce(0 second, actor, IndexAll)

    Await.result(system.whenTerminated, Duration.Inf)
  }

  def shutdown(): Future[Terminated] = system.terminate()
}