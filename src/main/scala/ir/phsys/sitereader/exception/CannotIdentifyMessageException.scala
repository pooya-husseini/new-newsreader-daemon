package ir.phsys.sitereader.exception

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 4/28/15
 *         Time: 6:49 PM
 */
case class CannotIdentifyMessageException(message:String) extends Exception(message)
