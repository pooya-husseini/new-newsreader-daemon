package ir.phsys.sitereader.engine.solr

import java.io.PrintWriter
import java.io.StringWriter

import akka.actor._
import ir.phsys.sitereader.engine.solr.SolrServer._
import ir.phsys.sitereader.exception.CannotIdentifyMessageException
import ir.phsys.sitereader.mail.SendMail
import ir.phsys.sitereader.solr.server.SolrServerFactory
import ir.phsys.sitereader.{ReceiveLogger, messages}
import org.apache.logging.log4j.LogManager
import org.apache.solr.client.solrj.SolrQuery
import org.apache.solr.client.solrj.SolrServerException
import org.apache.solr.client.solrj.impl.HttpSolrClient

import scala.collection.JavaConverters._
import scala.language.postfixOps
import scala.util.Failure
import scala.util.Success
import scala.util.Try

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 22.04.15
 *         Time: 18:01
 */
object SolrServer {
  private val logger = LogManager.getLogger(this.getClass)
  lazy val props = Props(new SolrServer)
  val collection: String = "collection1"
}


class SolrServer extends Actor with ActorLogging with ReceiveLogger {

  import messages.Messages._

//  val client: HttpSolrClient = SolrServerFactory.create(SolrServer.collection)

  override def receive: Receive = {
    case CommitBeans(beans, remains,startedTime) =>
      if (beans.nonEmpty) {
        val client: HttpSolrClient = SolrServerFactory.create(SolrServer.collection)
        beans.map(_.toSolrDocument).foreach(client.add)
        //        client.addBeans(beans.asJava)
        client.commit()
        //        val query = new SolrQuery("")

        logger.info(s"${beans.size} items has already committed!")
        client.close()
      }
      sender ! CommitmentSucceed(remains,startedTime)
    case Optimize=>
      val client: HttpSolrClient = SolrServerFactory.create(SolrServer.collection)
      client.optimize()
      client.close()
    case FilterNotExistKeys(keysMap, remains,startedTime) =>
      sender ! FilteredKey(keysMap -- filterExistKeys(keysMap.keySet), remains,startedTime)
    case x => throw CannotIdentifyMessageException(s"Can not identify $x from ${sender()}")
  }

  def filterExistKeys(keys: Set[String]): List[String] = {
    val client: HttpSolrClient = SolrServerFactory.create(SolrServer.collection)
    val query: SolrQuery = new SolrQuery

    val size = keys.size

    val grouped: List[Set[String]] = keys.grouped(10).toList

    val result = grouped.flatMap { group =>
      val keysString = group.mkString(" ")
      val queryString: String = s"id:($keysString) AND (archived_b:true OR updateCount:[4 TO *])"
      query.setQuery(queryString)
      query.setStart(0)
      query.setRows(10)
      query.setFields("id")
      Try {
        val response = client.query(query).getResults
        response.listIterator().asScala.map(_.getFieldValue("id").asInstanceOf[String]).toList

      } match{
        case Failure(f)=>
          logger.warn("An exception occurred, trying to send a mail!")
          val sw = new StringWriter
          f.printStackTrace(new PrintWriter(sw))
          SendMail("exception@phsys.ir","Solr server exception",sw.toString)
          throw f
        case Success(x)=>x
      }
    }
    client.close()
    result
  }

//  @throws[Exception](classOf[Exception])
//  override def postStop(): Unit = client.close()
}