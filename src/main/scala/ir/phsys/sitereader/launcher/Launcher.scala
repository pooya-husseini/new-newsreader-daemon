package ir.phsys.sitereader.launcher

import ir.phsys.sitereader.constants.Constants
import ir.phsys.sitereader.indexer.IndexRunner
import org.apache.logging.log4j.LogManager

import scala.util.{Failure, Success, Try}

/**
 * @author Pooya husseini on 9/3/15.
 *         for more information about the author @see pooya-hfp.ir
 */
object Launcher {
  private lazy val logger = LogManager.getLogger(this.getClass)

  def main(args: Array[String]):Unit = {
    args.foreach {
      case x:String if x.startsWith("-DreaderConfPath") =>
        val split: Array[String] = x.split("=")
        if (split.length > 1) {
          System.setProperty(Constants.confPathKey, split(1))
        } else {
          logger.warn("The configuration parameter is not correct, the programs uses the default configuration")
        }
      case x:Any => logger.warn(s"$x is not a valid parameter")
    }
    val runner: IndexRunner = new IndexRunner()
    Try {
      runner.init()
    } match {
      case Success(x) =>
        runner.shutdown()
      case Failure(t) =>
        logger.warn(t)
        runner.shutdown()
    }
  }
}