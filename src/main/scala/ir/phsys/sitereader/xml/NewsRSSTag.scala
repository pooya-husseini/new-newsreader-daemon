package ir.phsys.sitereader.xml

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 15.04.15
 *         Time: 20:24
 */
case class NewsRSSTag(link: String, pubDate: String, title: String, author: String)