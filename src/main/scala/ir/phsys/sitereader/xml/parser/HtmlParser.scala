package ir.phsys.sitereader.xml.parser

import java.net.{HttpURLConnection, URL}

import org.apache.logging.log4j.LogManager
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

import scala.io.Source
import scala.util.parsing.combinator.JavaTokenParsers
import scala.util.{Failure, Success, Try}

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
  *         Date: 18.04.15
  *         Time: 19:55
  */

class HtmlParser(urlAddress: String, followRedirects: Boolean) {
  private val agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36"


  def makeValidUrl(u: String): String = {

    def simpleMakeUrl(): String = {
      Try {
        new URL(u.trim).toURI.toASCIIString
      } match {
        case Success(x) => x
        case Failure(t) => u
      }
    }

    val contentUrl = new URL(u)

    val conn = contentUrl.openConnection()

    val httpConn = conn.asInstanceOf[HttpURLConnection]
    httpConn.setAllowUserInteraction(true)
    httpConn.setInstanceFollowRedirects(false)
    httpConn.setRequestMethod("GET")
    httpConn.setRequestProperty("User-Agent", agent)
    httpConn.setReadTimeout(20000)
    httpConn.connect()
    val result = if (httpConn.getResponseCode == 301) {

      HtmlParser.logger.warn(urlAddress + " is going to be redirect")
      val url = httpConn.getHeaderField("Location")
      if (url.startsWith("http")) {
        val locbytes = new Array[Byte](url.length())
        for (index <- locbytes.indices) {
          locbytes(index) = url.charAt(index).asInstanceOf[Byte]
        }
        val loc2 = new String(locbytes, "UTF-8")
        loc2
      } else {
        simpleMakeUrl()
      }
    } else {
      simpleMakeUrl()
    }

    httpConn.disconnect()

    result
  }

  def loadContent: Document = {

    val url = makeValidUrl(urlAddress.trim)
    val content = Source.fromInputStream(new URL(url).openStream()).mkString
    Jsoup.parse(content, url)

  }

  trait Tag

  case class SimpleTag(tagName: String) extends Tag

  case class AttributedTag(tagName: String, att: String, value: String) extends Tag

  //case class StyledTag(tagName:String,className:String) extends Tag
  //  case class ComplexTag(tag:List[Tag])

  class JqueryLikeSyntax extends JavaTokenParsers {
    private def token = ident | wholeNumber

    private def simpleTag = token ^^ {
      case x => SimpleTag(x)
    }

    private def attributedTag = token ~ "[" ~ token ~ "=" ~ token ~ "]" ^^ {
      case a ~ "[" ~ b ~ "=" ~ c ~ "]" => AttributedTag(a, b, c)

    }

    private def styledTag = token ~ "." ~ token ^^ {
      case a ~ "." ~ b => AttributedTag(a, "class", b)
    }

    private def idTag = token ~ "#" ~ token ^^ {

      case a ~ "#" ~ b => AttributedTag(a, "id", b)
    }

    def tag = styledTag | idTag | attributedTag | simpleTag

    def flowPattern = (tag ~ rep(">" ~ tag)) ^^ {
      case x ~ xs => x :: xs.map {
        case ">" ~ z => z
      }
    }

    def startParsing(s: String): List[Tag] = {
      parseAll(flowPattern, s) match {
        case Success(x, _) => x
      }
    }
  }

  def extract(filters: List[String]): String = {

    val xml = loadContent
    filters.map(xml.select).map(_.text()).mkString
  }
}

object HtmlParser {
  private val logger = LogManager.getLogger(classOf[HtmlParser])

}