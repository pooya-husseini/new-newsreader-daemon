package ir.phsys.sitereader

import java.util.Date

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: ${Date}
 *         Time: ${time}
 */

trait Content

case class EmptySiteContent(title: String, id: String, publisher: String, link: String, url: String, pubDate: Date) extends Content
case class SiteContent(title: String, id: String, content: String, publisher: String, link: String, url: String, pubDate: Date) extends Content
case class FailedSiteContent(title: String, id: String, content: String, publisher: String, link: String, url: String, pubDate: Date) extends Content
