package ir.phsys.sitereader.mail

import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader

import scala.io.Source

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 9/29/15
 *         Time: 7:44 PM
 */
object SendMail {

  def apply(address: String, subject: String, message: String): Unit = {
    val tempFile: File = File.createTempFile("file", "temp")
    printToFile(tempFile){
      x=> x.print(message)
    }

    val pb=new ProcessBuilder()

    val command: String = s"""cat ${tempFile.getAbsolutePath} | mail -s "$subject" $address"""
    pb.command(command)
    pb.redirectOutput(new File("log.txt"))
    pb.start()

//    val process: Process = Runtime.getRuntime.exec(command)
//    process.waitFor()
    tempFile.delete()
  }

  def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
    val p = new java.io.PrintWriter(f)
    try { op(p) } finally { p.close() }
  }
}
