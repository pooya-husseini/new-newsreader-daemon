package ir.phsys.sitereader.constants

/**
 * @author Pooya husseini on 9/3/15.
 *         for more information about the author @see pooya-hfp.ir
 */
object Constants {
  lazy val confPathKey = "reader.conf.path"
  lazy val defaultConfPath = {
    Option(System.getProperty(confPathKey)) match {
      case None =>
        "classpath:/readers.groovy"
      case Some(x) =>
        s"file:$x"
    }
  }
}
