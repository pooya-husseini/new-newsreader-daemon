package ir.phsys.sitereader

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import ir.phsys.sitereader.SiteReaderActor.SiteReaderActorMetaData
import ir.phsys.sitereader.SiteReaderActor.loadReaders
import ir.phsys.sitereader.beans.AgencyDto
import ir.phsys.sitereader.constants.Constants
import ir.phsys.sitereader.contentmaker.ContentMakerActor
import ir.phsys.sitereader.engine.solr.SolrServer
import ir.phsys.sitereader.exception.CannotIdentifyMessageException
import ir.phsys.sitereader.messages.Messages._
import ir.phsys.sitereader.util.parser.{ConfigParser, Reader}
import org.apache.logging.log4j.LogManager

import scala.io.Source

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 22.04.15
 *         Time: 18:21
 */


trait ReceiveLogger {
  this: Actor with ActorLogging =>

  def logMessage: Receive = new Receive {
    def isDefinedAt(x: Any): Boolean = {
      log.debug(s"Got a $x")
      false
    }

    def apply(x: Any): Unit = throw new UnsupportedOperationException
  }
}

class SiteReaderActor extends Actor with ActorLogging with ReceiveLogger {

  lazy val solrServer: ActorRef = {
    context.actorOf(SolrServer.props)
  }

  lazy val siteReaders: Map[String, SiteReaderActorMetaData] = loadReaders.map {
    case r@Reader(name, urls, tag, dateFormat, selects, f, readHttpFullContent) =>
      (tag, SiteReaderActorMetaData(context.actorOf(ContentMakerActor.props(solrServer,
        new AgencyDto(tag, name), dateFormat, selects, f), tag), r))
  }.toMap

  override def receive: Receive = logMessage orElse {
    case StartIndex(agencyTag, tags, remains, startedTime) =>
      siteReaders(agencyTag).actor ! MakeContent(tags, remains, startedTime)
    case GiveTagUrls(tag, remains, startedTime) =>
      context.parent ! AgencyUrls(tag, siteReaders(tag).reader.urls, remains, startedTime)
    case x: ContentMakingResult =>
      context.parent ! x
    case x:Any => throw CannotIdentifyMessageException(s"Can not identify $x from ${sender()}")
  }
}

object SiteReaderActor {
  lazy val props = Props(new SiteReaderActor)

  case class SiteReaderActorMetaData(actor: ActorRef, reader: Reader)

  lazy val loadReaders: List[Reader] = {
    val str = if (Constants.defaultConfPath.startsWith("classpath:")) {
      val inputStream = getClass.getResourceAsStream(Constants.defaultConfPath.replace("classpath:", ""))
      val string = Source.fromInputStream(inputStream).mkString
      inputStream.close()
      string
    } else {
      Source.fromURL(Constants.defaultConfPath).mkString
    }
    ConfigParser.parse(str)
  }
}