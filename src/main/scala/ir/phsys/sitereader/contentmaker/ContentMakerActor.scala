package ir.phsys.sitereader.contentmaker

import java.io.PrintWriter
import java.io.StringWriter
import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorRef
import akka.actor.Props
import ir.phsys.sitereader.Content
import ir.phsys.sitereader.EmptySiteContent
import ir.phsys.sitereader.FailedSiteContent
import ir.phsys.sitereader.ReceiveLogger
import ir.phsys.sitereader.SiteContent
import ir.phsys.sitereader.beans.AgencyDto
import ir.phsys.sitereader.contentmaker.ContentMakerActor.logger
import ir.phsys.sitereader.exception.CannotIdentifyMessageException
import ir.phsys.sitereader.messages.Messages.ContentMakingFailed
import ir.phsys.sitereader.messages.Messages.ContentMakingFinished
import ir.phsys.sitereader.messages.Messages.FilterNotExistKeys
import ir.phsys.sitereader.messages.Messages.FilteredKey
import ir.phsys.sitereader.messages.Messages.MakeContent
import ir.phsys.sitereader.util.DateUtils
import ir.phsys.sitereader.util.StopWatch
import ir.phsys.sitereader.xml.NewsRSSTag
import ir.phsys.sitereader.xml.parser.HtmlParser
import org.apache.commons.codec.digest.DigestUtils
import org.apache.logging.log4j.LogManager

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.language.postfixOps
import scala.util.Failure
import scala.util.Success
import scala.util.Try

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 15.04.15
 *         Time: 19:47
 */

object ContentMakerActor {
  private val logger = LogManager.getLogger(this.getClass)

  def props(serverActor: ActorRef, agencyDto: AgencyDto, dateFormat: String, selects: List[String], followRedirects: Boolean): Props =
    Props(new ContentMakerActor(ContentMakerConf(serverActor, agencyDto, dateFormat, selects, followRedirects)))
}

case class ContentMakerConf(serverActor: ActorRef, agency: AgencyDto, dateFormat: String, selects: List[String], followRedirects: Boolean)

class ContentMakerActor(conf: ContentMakerConf) extends Actor with ActorLogging with ReceiveLogger {

  override def receive: Receive = logMessage orElse {
    case m@MakeContent(tags, remains, startedTime) =>
      Future {
        val contents = tags.map {
          case tag@(rssTag, url) =>
            val rawLink = rssTag.link
            (makeKey(rawLink), (NewsRSSTag(rawLink, rssTag.pubDate, rssTag.title, rssTag.author), url))
        }.toMap
        conf.serverActor ! FilterNotExistKeys(contents, remains, startedTime)
      }
    case FilteredKey(keysMap, remains, startedTime) =>
      val futures: List[Future[Content]] = keysMap.toList.map {
        case (key, (rssTag: NewsRSSTag, url: String)) =>
          Future {
            Try {
              StopWatch("Extraction") {
                val text = new HtmlParser(rssTag.link, conf.followRedirects).extract(conf.selects)
                val pubDate = makeDate(rssTag.pubDate)
                (text, pubDate)
              }
            } match {
              case Success((text, pubDate)) =>
                if (text.trim.isEmpty) {
                  logger.info(s"the text of this link is empty ${rssTag.link}")
                  EmptySiteContent(rssTag.title, key, rssTag.author, rssTag.link, url, pubDate)
                } else {
                  logger.info(s"step ${rssTag.link} of ${rssTag.author} processed")
                  SiteContent(rssTag.title, key, text, rssTag.author, rssTag.link, url, pubDate)
                }
              case Failure(t) =>
                logger.error(s"Exception occurred in extraction of ${rssTag.link} ", t)
                val sw = new StringWriter
                t.printStackTrace(new PrintWriter(sw))
                FailedSiteContent(rssTag.title, key, sw.toString, rssTag.author, rssTag.link, url, new Date())
            }
          }
      }
      Future.sequence(futures) onComplete {
        case Success(x) =>
          context.parent ! ContentMakingFinished(x, remains, startedTime)
        case Failure(t) =>
          context.parent ! ContentMakingFailed(t, remains, startedTime)
      }
    case x: Any => throw CannotIdentifyMessageException(s"Can not identify $x from ${sender()}")
  }

  private def makeDate(str: String): Date = Option(str) match {
    case None | Some("") =>
      DateUtils.startOfToday()
    case Some(x) =>
      val format = new SimpleDateFormat(conf.dateFormat)
      val date = format.parse(str.trim)
      val calendar= Calendar.getInstance()
      calendar.setTime(date)
      calendar.set(Calendar.MILLISECOND,0)
      calendar.getTime

  }

  def keyPrefix: String = this.getClass.getSimpleName

  private def makeKey(str: String): String = {
    DigestUtils.sha512Hex((keyPrefix + str).getBytes)
  }
}