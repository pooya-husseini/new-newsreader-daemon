package ir.phsys.sitereader.util

import java.util.{Date, Calendar, GregorianCalendar}

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 9/2/15
 *         Time: 4:09 PM
 */
object DateUtils {

  def startOfToday() = {
    startOfDay(new Date())
  }

  def endOfToday() = {
    endOfDay(new Date())
  }


  def startOfDay(date: java.util.Date, daysEarlier: Int = 0) = {
    val startOfDay: Calendar = new GregorianCalendar
    startOfDay.setTime(date)
    if (daysEarlier > 0) {
      startOfDay.add(Calendar.DAY_OF_MONTH, -daysEarlier)
    }
    startOfDay.set(Calendar.HOUR, 0)
    startOfDay.set(Calendar.AM_PM, Calendar.AM)
    startOfDay.set(Calendar.MINUTE, 0)
    startOfDay.set(Calendar.SECOND, 0)
    startOfDay.getTime

  }

  def endOfDay(date: java.util.Date) = {
    val endofDayTime=(11,59)
    val endOfDay: Calendar = new GregorianCalendar
    endOfDay.setTime(date)
    endOfDay.set(Calendar.HOUR, endofDayTime._1)
    endOfDay.set(Calendar.HOUR_OF_DAY, 11)
    endOfDay.set(Calendar.AM_PM, Calendar.PM)
    endOfDay.set(Calendar.MINUTE, 59)
    endOfDay.set(Calendar.SECOND, 59)
    endOfDay.getTime
  }
}
