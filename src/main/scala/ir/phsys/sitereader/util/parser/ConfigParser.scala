package ir.phsys.sitereader.util.parser


import org.apache.logging.log4j.LogManager

import scala.language.postfixOps
import scala.util.parsing.combinator.JavaTokenParsers


/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 5/19/15
 *         Time: 4:08 PM
 */
object ConfigParser extends JavaTokenParsers {
  private val logger = LogManager.getLogger(this.getClass)
  private val dateFormatKey: String = "dateFormat"
  private val selectsKey: String = "selects"
  private val urlsKey: String = "urls"
  private val followRedirectsKey: String = "followRedirects"
  private val nameKey: String = "name"
  private val readHttpFullContentKey: String = "readHttpFullContent"
  private val dateSelector: String = "dateSelector"

  def parse(str: String): List[Reader] = {
    parseAll(statement, str) match {
      case Success(result, _) => result
      case Failure(msg, _) =>
        logger.error("msg = " + msg)
        Nil
      case Error(msg, _) =>
        logger.error("msg = " + msg)
        Nil
    }
  }

  private def statement = rep(comment | reader) ^^ {
    case x:Any => x.filter {
      case _: Reader => true
      case _ => false
    }.map(_.asInstanceOf[Reader])
  }

  private def reader = ident ~ "{" ~ readerContent ~ "}" ^^ {
    case tag ~ "{" ~ x ~ "}" =>
      val urls = x.collect {
        case ListItem(`urlsKey`, y) => y
      }.head

      val name = x.collect {
        case SimpleItem(`nameKey`, y) => y
      }.head

      val dateFormat = x.collect {
        case SimpleItem(`dateFormatKey`, y) => y
      }.head

      val dateSelect = x.collect {
        case SimpleItem(`dateSelector`, y) => y
      } match {
        case item if item.size <= 0 => None
        case item :: xs => Some(item)
      }

      val follow = x.collect {
        case SimpleItem(`followRedirectsKey`, y) => y
      } match {
        case item if item.size <= 0 => false
        case item :: xs => item.toBoolean
      }

      val readFull = x.collect {
        case SimpleItem(`readHttpFullContentKey`, y) => y
      } match {
        case item if item.size <= 0 => false
        case item :: xs => item.toBoolean
      }

      val selects = x.collect {
        case ListItem(`selectsKey`, y) => y
      }.head
      Reader(name, urls, tag ,dateFormat, selects, follow, readFull)
  }

  private def comment = "//" ~ ".*".r ^^ {
    case x:Any => Comment
  }

  private val boolean = "true" | "false"

  private def readerContent = rep(urls | name | dateFormat | selects | comment | followRedirects | readHttpFullContent)

  private def name = nameKey ~ "=" ~ stringLiteral ^^ {
    case a ~ "=" ~ x => SimpleItem(nameKey, x.apply(1, -1))
  }

  private def urls = urlsKey ~ "=" ~ "[" ~ repsep(stringLiteral, ",") ~ "]" ^^ {
    case a ~ "=" ~ "[" ~ x ~ "]" => ListItem(urlsKey, x.map(item => item.apply(1, -1)))
  }

  //  def tag = "tag" ~ "=" ~ stringLiteral ^^ {
  //    case "tag" ~ "=" ~ x => Tag(x)
  //  }

  private def dateFormat = dateFormatKey ~ "=" ~ stringLiteral ^^ {
    case a ~ "=" ~ x => SimpleItem(dateFormatKey, x.apply(1, -1))
  }

  private def dateSelectorRule = dateSelector ~ "=" ~ stringLiteral ^^ {
    case a ~ "=" ~ x => SimpleItem(dateSelector, x)
  }

  private def readHttpFullContent = readHttpFullContentKey ~ "=" ~ boolean ^^ {
    case a ~ "=" ~ x => SimpleItem(readHttpFullContentKey, x)
  }

  private def followRedirects = followRedirectsKey ~ "=" ~ boolean ^^ {
    case a ~ "=" ~ x => SimpleItem(followRedirectsKey, x)
  }

  private def selects = selectsKey ~ "=" ~ "[" ~ repsep(stringLiteral, ",") ~ "]" ^^ {
    case a ~ "=" ~ "[" ~ x ~ "]" => ListItem(selectsKey, x.map(item => item.apply(1, -1)))
  }

  implicit class StringIndexEnhancer(str: String) {
    def getCharAt(i: Int): Char = if (i >= 0) str.apply(i) else str.apply(str.length + i)

    def apply(from: Int, to: Int): String = str.substring(from, if (to < 0) str.length + to else to)
  }

}


case class Reader(name: String,
                  urls: List[String],
                  tag: String,
                  dateFormat: String,
                  selects: List[String],
                  followRedirects: Boolean = false,
                  readHttpFullContent: Boolean = false
                   )

case class ListItem(name: String, arg: List[String])

case class SimpleItem(name: String, value: String)

case object Comment
