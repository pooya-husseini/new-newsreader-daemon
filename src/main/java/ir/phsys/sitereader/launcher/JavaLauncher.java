package ir.phsys.sitereader.launcher;

import ir.phsys.sitereader.indexer.IndexRunner;

/**
 * @author Pooya husseini on 9/1/15.
 *         for more information about the author @see pooya-hfp.ir
 */
public class JavaLauncher {
    public static void main(String[] args) {
//        System.out.println(Arrays.toString(args));
        new IndexRunner().init();
    }
}
