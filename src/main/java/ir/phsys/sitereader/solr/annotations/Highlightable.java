package ir.phsys.sitereader.solr.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 8/30/15
 *         Time: 4:08 PM
 */


@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Highlightable {
}
