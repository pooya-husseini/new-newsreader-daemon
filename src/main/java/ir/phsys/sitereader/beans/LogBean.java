package ir.phsys.sitereader.beans;

import org.apache.solr.client.solrj.beans.Field;

import java.util.Date;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 8/10/15
 *         Time: 5:39 PM
 */
public class LogBean {
    private Date date;
    private String content;


    public LogBean(Date date, String content) {
        this.date = date;
        this.content = content;
    }


    public Date getDate() {
        return date;
    }

    @Field("date_dt")
    public void setDate(Date date) {
        this.date = date;
    }


    public String getContent() {
        return content;
    }

    @Field("content_s")
    public void setContent(String content) {
        this.content = content;
    }
}
