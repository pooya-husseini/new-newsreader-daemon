package ir.phsys.sitereader.beans;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 8/9/15
 *         Time: 12:34 PM
 */
public class AgencyDto {
    private String tag;
    private String name;
    private Long failureCount;
    private Long successCount;
    private Long emptyCount;


    public AgencyDto() {

    }
    public AgencyDto(String tag, String name) {
        this.tag = tag;
        this.name = name;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getFailureCount() {
        return failureCount;
    }

    public void setFailureCount(Long failureCount) {
        this.failureCount = failureCount;
    }

    public Long getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Long successCount) {
        this.successCount = successCount;
    }

    public Long getEmptyCount() {
        return emptyCount;
    }

    public void setEmptyCount(Long emptyCount) {
        this.emptyCount = emptyCount;
    }
}
