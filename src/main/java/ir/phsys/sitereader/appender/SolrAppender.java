package ir.phsys.sitereader.appender;

import ir.phsys.sitereader.beans.LogBean;
import ir.phsys.sitereader.solr.server.SolrServerFactory;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 9/29/15
 *         Time: 6:15 PM
 */
@Plugin(name = "SolrAppender", category = "Core", elementType = "appender", printObject = true)
public class SolrAppender extends AbstractAppender {
    private static final String collection = "collection2";
    private final ReadWriteLock rwLock = new ReentrantReadWriteLock();
    private final Lock readLock = rwLock.readLock();

//    HttpSolrClient httpSolrClient;
//    {
//        try {
//            httpSolrClient = SolrServerFactory.create(collection);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    protected SolrAppender(String name, Filter filter, Layout<? extends Serializable> layout, final boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
    }

    @PluginFactory
    public static SolrAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("Layout") Layout<? extends Serializable> layout,
            @PluginElement("Filter") final Filter filter,
            @PluginAttribute("otherAttribute") String otherAttribute) {
        if (name == null) {
            LOGGER.error("No name provided for MyCustomAppenderImpl");
            return null;
        }
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        return new SolrAppender(name, filter, layout, true);
    }

    @Override
    public void append(LogEvent event) {

        readLock.lock();
        try {
            HttpSolrClient httpSolrClient=SolrServerFactory.create(collection);
            final byte[] bytes = getLayout().toByteArray(event);
            LogBean logBean = new LogBean(new Date(), new String(bytes));
            httpSolrClient.addBean(logBean);
            httpSolrClient.commit();
            httpSolrClient.close();
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public void stop() {
        super.stop();
//        try {
//            httpSolrClient.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }
}

