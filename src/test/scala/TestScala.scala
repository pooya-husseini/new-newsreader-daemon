import java.net.URL

import org.jsoup.Jsoup
import org.jsoup.select.Elements

import scala.io.Source

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 5/19/15
 *         Time: 7:58 PM
 */
object TestScala {

  def main(args: Array[String]): Unit = {
    val s: String = "http://www.yjc.ir/fa/news/5207228/"
    val urlStreamd=new URL(s).openStream()
    val content=Source.fromInputStream(urlStreamd).mkString
    urlStreamd.close()
    val select: Elements = Jsoup.parse(content).select("div.body")
    println(select.text())
  }
}
